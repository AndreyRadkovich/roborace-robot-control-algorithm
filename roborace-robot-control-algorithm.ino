#include <Servo.h>
#include <math.h>
#include <SoftwareSerial.h>   //Software Serial Port
#define RxD 10
#define TxD 11

float radian = 57.29f;

void setupBlueToothConnection( )
{
	blueToothSerial.begin( 38400 ); //Set BluetoothBee BaudRate to default baud rate 38400
	blueToothSerial.print( "\r\n+STWMOD=0\r\n" ); //set the bluetooth work in slave mode
	blueToothSerial.print( "\r\n+STNA=SeeedBTSlave\r\n" ); //set the bluetooth name as "SeeedBTSlave"
	blueToothSerial.print( "\r\n+STPIN=0000\r\n" );//Set SLAVE pincode"0000"
	blueToothSerial.print( "\r\n+STOAUT=1\r\n" ); // Permit Paired device to connect me
	blueToothSerial.print( "\r\n+STAUTO=0\r\n" ); // Auto-connection should be forbidden here
	delay( 2000 ); // This delay is required.
	blueToothSerial.print( "\r\n+INQ=1\r\n" ); //make the slave bluetooth inquirable 
	Serial.println( "The slave bluetooth is inquirable!" );
	delay( 4000 ); // This delay is required.
	blueToothSerial.flush( );
}

class Drive
{
public:
	enum Direction { FORWARD = LOW, BACKWARD = HIGH };
  
public:
	Drive(int dirPin, int powerPin )
	{
		this->dirPin = dirPin;
		this->powerPin = powerPin;    
		dir = FORWARD;
		power = 0;
    
		pinMode( dirPin, OUTPUT );
		pinMode( powerPin, OUTPUT );
    
		analogWrite( powerPin, power );
		digitalWrite( dirPin, FORWARD );
	}
  
	void setPower( int power )
	{
		this->power = power;
		analogWrite( powerPin, power );
	}
        void stop()
        {
                setPower( 0 );
        }
	void setDirection( Direction dir )
	{
		if( dir != this->dir )
		{
			analogWrite( powerPin, 0 );
			delay( 5 );
			digitalWrite( dirPin, dir );
			analogWrite( powerPin, power );
			this->dir = dir;
		}
	}
 
	Direction getDirection()
	{
		return dir;
	}
	
	void setMaxSpeed(int max)
	{
		maxSpeed = max;
	}
	
	void setMinSpeed(int min)
	{
		minSpeed = min;
	}

        float getMaxSpeed()
        {
                return maxSpeed;
        }
        
        float getMinSpeed()
        {
                return minSpeed;
        }

private:
	int dirPin, powerPin;
	Direction dir;
	int power;
	int maxSpeed, minSpeed;
};

class IRSensor
{
public:
	IRSensor()
	{
		pin = 0;
                hiddenDistance = 0;
                thereticalDistance =  65;
	};

	void attach( int pin )
	{
		this->pin = pin;
		valtage = 5.0;
		pinMode( pin, INPUT );
	}

	void setWorkingVoltage( float voltage )
	{
		this->valtage = valtage;
	}

	void setHiddenDistance(float d)
	{
			hiddenDistance = d;
	}
        
	void setThereticalDistance(float d)
	{
			thereticalDistance = d;
	}

	float DistanceCM()
	{
		float volts = analogRead( pin ) * valtage / 1024.0f;
		return thereticalDistance * pow( volts, -1.10 ) - hiddenDistance;
	}

private:
	int pin;
	float valtage;
	float hiddenDistance;
	float thereticalDistance;
};

class Distance
{
public:
	float rightFront;
	float rightBack;
	float right45;
	float left45;
	float front;
public:
	Distance()
	{
		right45 = left45 = front = rightBack = rightFront = 0;
	}
};

class Robot
{
private:
	Drive * drive;
	Servo servo;
	IRSensor sharpSensorRightFront;
	IRSensor sharpSensorRightBack;
	IRSensor sharpSensorFront;
	IRSensor sharpSensorLeft;
	IRSensor sharpSensorRight;
	
	float d;
	
public:
	Robot()
	{
		sharpSensorRightFront.setWorkingVoltage( 5.0f );
		sharpSensorRightFront.attach( A1 );
		sharpSensorRightFront.setHiddenDistance( 12 );
		sharpSensorRightFront.setThereticalDistance( 26 );
		
		sharpSensorRightBack.setWorkingVoltage( 5.0f );
		sharpSensorRightBack.attach( A4 );
		sharpSensorRightBack.setHiddenDistance( 12 );
		sharpSensorRightBack.setThereticalDistance( 26 );
		
		sharpSensorFront.setWorkingVoltage( 5.0f );
		sharpSensorFront.attach( A3 );
		sharpSensorFront.setHiddenDistance( 13 );
		sharpSensorFront.setThereticalDistance( 65 );
		
		sharpSensorLeft.setWorkingVoltage( 5.0f );
		sharpSensorLeft.attach( A2 );
		sharpSensorLeft.setHiddenDistance( 17 );
		sharpSensorLeft.setThereticalDistance( 26 );
		
		sharpSensorRight.setWorkingVoltage( 5.0f );
		sharpSensorRight.attach( A0 );
		sharpSensorRight.setHiddenDistance( 19 );
		sharpSensorRight.setThereticalDistance( 26 );
		

		drive = new Drive( 5, 6 );
		drive->setDirection( Drive::FORWARD );
		drive->setPower( 90 );
		drive->setMaxSpeed( 200 );
		drive->setMinSpeed( 100 );
	  
		servo.attach( 11 );
		servo.write( 85 );
		pinMode(13, OUTPUT);
		
		d = 22.5f;
	}
	
	void getFrontDistance(float &result, int numOfMeasurements)
	{
		result = 0;
		for(int i = 0; i < numOfMeasurements; i++)
		{
			result += sharpSensorFront.DistanceCM();
		}
		result /= numOfMeasurements;
	}
	
	void getRightFrontDistance(float &result, int numOfMeasurements)
	{
		result = 0;
		for(int i = 0; i < numOfMeasurements; i++)
		{
			result += sharpSensorRightFront.DistanceCM();
		}
		result /= numOfMeasurements;
	}
	
	void getRightBackDistance(float &result, int numOfMeasurements)
	{
	  	result = 0;
		for(int i = 0; i < numOfMeasurements; i++)
		{
			result += sharpSensorRightBack.DistanceCM();
		}
		result /= numOfMeasurements;
	}
	
	void getRight45Distance(float &result, int numOfMeasurements)
	{
	  	result = 0;
		for(int i = 0; i < numOfMeasurements; i++)
		{
			result += sharpSensorRight.DistanceCM();
		}
		result /= numOfMeasurements;
	}
	
	void getLeft45Distance(float &result, int numOfMeasurements)
	{
	  	result = 0;
		for(int i = 0; i < numOfMeasurements; i++)
		{
			result += sharpSensorLeft.DistanceCM();
		}
		result /= numOfMeasurements;
	}
	
	void moveForwardWithMaxSpeed()
	{
		drive->setPower( drive->getMaxSpeed() );
	}
	
	void moveForward(int speed)
	{
		drive->setPower( speed );
	}
	
	void moveBackward(int speed)
	{
		drive->setDirection( Drive::BACKWARD );
		drive->setPower( speed );
	}
	
	void stop()
	{
		drive->stop();
	}
	
	void turnLeft(float degree)
	{
		servo.write( 85 + degree );
		drive->setPower((degree / 90) * ( drive->getMaxSpeed() - drive->getMinSpeed()) + drive->getMinSpeed());
	}

	void turnRight(float degree)
	{
		servo.write( 85 - degree );
		drive->setPower((degree / 90) * ( drive->getMaxSpeed() - drive->getMinSpeed()) + drive->getMinSpeed());
	}

	void turnCenter()
	{
	      servo.write( 85 ); 
	      moveForwardWithMaxSpeed();
	}
        
        float getDistanceBetweenRightSensors()
        {
              return d;
        } 
};

Robot * robot;

SoftwareSerial blueToothSerial( RxD, TxD );

void setup()
{
	robot = new Robot();
	pinMode( RxD, INPUT );
	pinMode( TxD, OUTPUT );
	setupBlueToothConnection( );
	Serial.begin( 9600 );
}

float distanceFrontPrev = 0;

void loop()
{
	digitalWrite(13, LOW);
	int N = 10;
	Distance distance;
	float   offset, 		//разница между показаниями distanceRightFront и distanceRightBack
		degree, 		//угол между направлением машины и стеной
		minimalDistance;	//минимальное расстояние до стены (измеряется от

	robot->getFrontDistance( distance.front, N);
	robot->getRightBackDistance( distance.rightBack, N);
	robot->getRightFrontDistance( distance.rightFront, N);
	robot->getLeft45Distance( distance.left45, N);
	robot->getRight45Distance( distance.right45, N);
	
	//разница между показаниями датчиков по правому борту
	offset = distance.rightBack - distance.rightFront;
        
	//вычисляем угол между направлением робота и стеной справа
	degree = atan( offset / robot->getDistanceBetweenRightSensors() ) * radian;
	
	//определяем минимальное расстояние до стены справа
	if ( degree < 0 ) degree += 180;
	if ( distance.rightFront < distance.rightBack )
		minimalDistance = abs(distance.rightFront * sin( 90 / radian - degree / radian ));
	else 	minimalDistance = abs(distance.rightBack * sin( 90 / radian - degree / radian ));
	

	Serial.print("front = ");
	Serial.print(distance.front);
	Serial.print("; right back = ");
	Serial.print(distance.rightBack);
	Serial.print("; right front = ");
	Serial.println(distance.rightFront);
/*	Serial.print("; minimal = ");
	Serial.print(minimalDistance);
	Serial.print("; degree = ");
	Serial.println(degree);
*/
        
        if ( distance.front <= 15.0f )
        {
                Serial.print("distance front = ");
                Serial.println(distance.front);
                digitalWrite(13, HIGH);
                if ( minimalDistance < 15.0f )
                {
                        Serial.print("minimal = ");
                        Serial.println(minimalDistance);
                        Serial.println("move backward");
                        robot->turnRight( 50 );
                        robot->moveBackward( 50 );
                        while( distance.front < 15.0f )
                        {
                                distance.front = 0;
                                robot->getFrontDistance(distance.front, N);
                                delay( 80 );
                        }
                        robot->moveForwardWithMaxSpeed();
                }
                else
                {
                        Serial.println("stop");
                        robot->stop();
                }
        }
	else
	{
		robot->moveForwardWithMaxSpeed();
		Serial.println("move forward with max speed");
		
		if( distance.front >= 75 )
		{	
            Serial.print( "distance front >= 75 \n degree = ");
            Serial.println(degree);
			if ( degree > 90 )
			{
				Serial.println("turn left");
				robot->turnLeft( degree );
			}
			else if ( degree < 90 )
			{
                Serial.println("turn right");
				robot->turnRight(degree - 90);
			}
			else
			{
                Serial.println("turn center");
				robot->turnCenter();
			}
		}
		else
		{
			Serial.println( "distance front < 75");
			float c = sqrt(distance.left45 * distance.left45 + distance.front * distance.front - 2 * distance.left45 * distance.front * 0.7f);
			float turnDegree = asin( (distance.left45 * 0.7f) / c ) * radian;
			if ( turnDegree >= 90 )
			{
				Serial.println("turn right");
				robot->turnRight( turnDegree );
			}
			else if ( turnDegree < 90 )
			{
				Serial.println("turn left");
				robot->turnLeft( 180 - turnDegree );
			}
		}
	}	
	Serial.println("---------------------------------------------");	
	//delay( 1250 );
}